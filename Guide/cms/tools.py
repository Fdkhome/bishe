import requests

HEADER = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
}


def Check_Url(url):
    res = requests.get(url, headers=HEADER)
    if res.status_code != 200:
        return False
    return True


# check_url('http://hao.uoo2.com/Index/program.html')