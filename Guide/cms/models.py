import datetime

from exts import db,bcrypt
from flask_login import AnonymousUserMixin


# role与user 多对多关系表

roles = db.Table(
    'role_users',
    db.Column('user_id', db.Integer, db.ForeignKey('admin_user.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('admin_role.id')),
)


# 创建第一个模型
# Flask Admin 用户
class User(db.Model):
    # 默认 表名 是类名小写版本；使用这个函数，指定表名
    __tablename__ = 'admin_user'
    # 每个 模型 必须有主键
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    email = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    enabled = db.Column(db.Boolean(), default=0)  # 是否激活,暂时设置为自动激活
    realname = db.Column(db.String(30))  # 真实姓名

    create_time = db.Column(db.DateTime, default=datetime.datetime.now)

    # 与roles的多对多外键
    roles = db.relationship(
        'Role',
        secondary=roles,
        backref=db.backref('users', lazy='dynamic')
    )

    def __init__(self):
        default = Role.query.filter_by(name="default").one()
        self.roles.append(default)


    def __repr__(self):
        return "<User '{}'>".format(self.email)

    def set_password(self, password):
        """ 设置 password 加密 """
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        """ 把 传入的password加密，并与成员 password比较 """
        return bcrypt.check_password_hash(self.password, password)

    def is_authenticated(self):
        """ Flask_Login 需要：表示User对象是否已登录
            Flask Login中，网站上每个用户都是从某些基础的用户对象继承而来，默认他们继承 AnonymousUserMixin 对象
        """
        if isinstance(self, AnonymousUserMixin):
            return False
        else:
            return True

    def is_active(self):
        """ Flask_Login 需要：是否通过了某种激活流程 """
        return self.enabled
        # return True

    def is_anonymous(self):
        """ Flask_Login 需要：访问者是否处于未登录的匿名状态 """
        if isinstance(self, AnonymousUserMixin):
            return True
        else:
            return False

    def get_id(self):
        """ Flask_Login 需要：返回User对象的唯一识别标志，即一个unicode字符串 """
        return str(self.id)  # python 2: return unicode(self.id)


# Flask Principal 角色
class Role(db.Model):
    __tablename__ = 'admin_role'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Role {}'.format(self.name)