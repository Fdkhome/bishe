from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from cms.models import User
from wtforms.validators import Email,EqualTo,DataRequired,length


class LoginForm(FlaskForm):
    email = StringField('邮箱', validators=[DataRequired(),Email(message='请输入正确的邮箱地址！')])
    password = PasswordField('密码', validators=[DataRequired(), length(max=255)])
    remember = BooleanField('记住我')  # 用户选择 是否记住用户登录状态

    def validate(self):
        check_validate = super(LoginForm, self).validate()
        # 如果验证未通过
        if not check_validate:
            return False

        # 检查是否有该用户名用户
        user = User.query.filter_by(email=self.email.data).first()
        if not user:
            self.email.errors.append(
                '用户名或密码错误！'
            )
            return False
        # 检查密码是否匹配
        if not user.check_password(self.password.data):
            self.email.errors.append(
                '用户名或密码错误！'
            )
            return False
        # 检查账号是否激活
        if not user.enabled:
            self.email.errors.append(
                '账号未激活，请联系管理员'
            )

            return False

        return True


# main  注册 表单
class RegisterForm(FlaskForm):
    email = StringField('邮箱', validators=[DataRequired(),Email(message='请输入正确的邮箱地址！')])
    realname = StringField('姓名', validators=[DataRequired(), length(max=30)])
    password = PasswordField('密码', validators=[DataRequired(), length(min=6,max=15 , message='密码长度在6-15个字符之间')])
    repassword = PasswordField('重复密码', validators=[DataRequired(), EqualTo('password')])

    def validate(self):
        check_validate = super(RegisterForm, self).validate()
        # 如果验证未通过
        if not check_validate:
            return False
        # 用户名存在，已被使用
        user = User.query.filter_by(realname=self.realname.data).first()
        if user:
            self.realname.errors.append("用户名已被使用！")
            return False
        return True


# main  修改密码 表单
class ChangePassForm(FlaskForm):
    email = StringField('邮箱', validators=[DataRequired(),Email(message='请输入正确的邮箱地址！')])
    old_password = PasswordField('旧密码', validators=[DataRequired(), length(min=6)])
    password = PasswordField('新密码', validators=[DataRequired(), length(min=6)])
    repassword = PasswordField('确认新密码', validators=[DataRequired(), EqualTo('password')])

    def validate(self):
        check_validate = super(ChangePassForm, self).validate()
        # 如果验证未通过
        if not check_validate:
            return False
        # 用户名 与 密码相符
        user = User.query.filter_by(email=self.email.data).first()
        if user:
            if user.check_password(self.old_password.data):
                return True
            else:
                self.old_password.errors.append("旧密码错误！")
                return False
        else:
            self.old_password.errors.append("用户名错误！")
            return False