from flask import Blueprint, render_template, redirect, url_for, session, g, flash, request, jsonify, current_app
from flask_login import login_required, logout_user, login_user, current_user
from flask_principal import identity_changed, Identity
import logging
import config

from cms.form import LoginForm, RegisterForm, ChangePassForm
from cms.models import User
from cms.tools import Check_Url
from common.models import Classify, WebSite
from exts import db, admin_permission

cms_bluePrint = Blueprint('cms',__name__,static_folder='templates/cms',url_prefix='/cms')


@cms_bluePrint.before_request
def before_request():
    if current_user.is_authenticated:
        if admin_permission.can():
            g.is_admin = True
        else:
            g.is_admin = False
        g.user = current_user
    else:
        g.user = None


@cms_bluePrint.route('/login/', methods=['GET','POST'])
def login():
    logging.info('CMS用户‘{}’进行了登录操作'.format(current_user.email))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        login_user(user, remember=form.remember.data)

        # flask_principal 登录后，触发 on_identity_loaded 方法，创建Need 对象
        identity_changed.send(
            current_app._get_current_object(),
            identity=Identity(user.id)
        )
        # session['email'] = user.email
        return redirect(url_for('cms.index'))

    return render_template('cms/login.html',form=form)


@cms_bluePrint.route('/logout/', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('cms.login'))


@cms_bluePrint.route('/regist/', methods=['GET','POST'])
def regist():
    logging.info('CMS用户注册')
    form = RegisterForm()
    if form.validate_on_submit():
        user = User()
        user.email = form.email.data
        user.realname = form.realname.data
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('cms.login'))
    return render_template('cms/regist.html',form=form)


@cms_bluePrint.route('/changepass', methods=['GET', 'POST'])
def changepass():
    """ 修改密码 """
    logging.info('修改密码')
    form = ChangePassForm()
    if form.validate_on_submit():
        new_user = User.query.filter_by(email=form.email.data).first()
        new_user.set_password(form.password.data)
        db.session.add(new_user)
        db.session.commit()

        flash("密码已成功修改， 请重新登陆。", category="success")
        return redirect(url_for('cms.login'))
    return render_template('cms/changepass.html', form=form)


@cms_bluePrint.route('/index/', methods=['GET'])
@login_required
def index():
    logging.info('进入CMS管理主页')
    form = {}
    classifys = db.session.query(Classify).order_by(Classify.weight.desc()).order_by(Classify.create_time.desc()).all()
    webs = db.session.query(WebSite).order_by(WebSite.create_time.desc())
    category = request.args.get('category')
    pageno = request.args.get('pageno')
    status = request.args.get('status')
    website_name = request.args.get('website_name')
    form['category'] = category
    form['status'] = status
    form['website_name'] = website_name
    if category:
        webs = webs.filter(WebSite.website_classify_id == category)
    if status:
        webs = webs.filter(WebSite.website_status == int(status))
    if website_name:
        webs = webs.filter(WebSite.website_name.ilike('%'+website_name+'%'))

    page = 1
    count = webs.count()
    if pageno:
        page = int(pageno)

    webs = webs.paginate(page=page, per_page=config.PAGE_LIMIT)

    content = {
        'classifys' : classifys,
        'webs': webs,
        'form':form,
        'count' : count,
        'limit': config.PAGE_LIMIT,
        'curr' : page
    }
    return render_template('cms/index.html', **content)


@cms_bluePrint.route('/AddWebInfo/', methods=['POST'])
@login_required
def AddWebInfo():

    category1 = request.form.get('category1')
    web_name = request.form.get('web_name')
    web_url = request.form.get('web_url')
    web_intro = request.form.get('web_intro')
    logging.info('添加了《{}》的网站信息'.format(web_name))
    web = WebSite(website_name=web_name, website_url=web_url, website_intro=web_intro, website_status=0, website_classify_id=int(category1))
    db.session.add(web)
    db.session.commit()
    return redirect(url_for('cms.index'))


@cms_bluePrint.route('/Upline/', methods=['POST'])
@login_required
def Upline():
    logging.info('进入了网站上线的操作！')
    web_id = request.form.get('web_id')
    web = WebSite.query.filter(WebSite.id == web_id).first()
    if web and web.website_status == 0:
        web.website_status = 1
        db.session.commit()
        logging.info('进行了《{}》网站的上线操作！'.format(web.website_name))
        return jsonify({'code':200,'msg':'上线成功！'})
    else:
        return jsonify({'code':404,'msg':'上线失败'})


@cms_bluePrint.route('/Downline/', methods=['POST'])
@login_required
def Downline():
    logging.info('进入了网站下线的视图函数！')
    web_id = request.form.get('web_id')
    web = WebSite.query.filter(WebSite.id == web_id).first()
    if web and web.website_status == 1:
        web.website_status = 0
        db.session.commit()
        logging.info('进行了《{}》网站的下线操作！'.format(web.website_name))
        return jsonify({'code':200,'msg':'下线成功！'})
    else:
        return jsonify({'code':404,'msg':'下线失败'})


@cms_bluePrint.route('/UpdateWeb/', methods=['POST'])
@login_required
def UpdateWeb():
    logging.info('进入了网站信息修改的视图函数！')
    web_id = request.form.get('web_id')
    category2 = request.form.get('category2')
    web_name = request.form.get('web_name')
    web_url = request.form.get('web_url')
    web_intro = request.form.get('web_intro')
    web = WebSite.query.filter(WebSite.id == web_id).first()
    if web:
        web.website_classify_id = category2
        web.website_name = web_name
        web.website_url = web_url
        web.website_intro = web_intro
        db.session.commit()
        logging.info('修改《》网站信息成功！'.format(web.website_name))
        flash('修改网站信息成功！')
        return redirect(url_for('cms.index'))
    else:
        flash('网站不存在！')
        return redirect(url_for('cms.index'))


@cms_bluePrint.route('/Delwebsite/', methods=['POST'])
@login_required
def Delwebsite():
    id = request.form.get('id')
    web = WebSite.query.filter(WebSite.id == id).first()
    if web:
        db.session.delete(web)
        db.session.commit()
        logging.info('删除《》网站信息成功！'.format(web.website_name))
        return jsonify({'code':200,'msg':'删除成功！'})
    else:
        return jsonify({'code':404,'msg':'删除失败！'})


@cms_bluePrint.route('/classify/')
@login_required
def classify():
    logging.info('进入了网站分类管理的视图函数！')
    classifys = Classify.query.order_by(Classify.weight.desc()).order_by(Classify.create_time.desc()).all()
    content = {
        'classifys': classifys
    }
    return render_template('cms/classify.html', **content)


@cms_bluePrint.route('/AddClassify/', methods=['POST'])
@login_required
def AddClassify():
    classify_name = request.form.get('classify_name')
    weight = request.form.get('weight')
    logging.info('添加了新的网站分类《%s》，权重为%s'%(classify_name, weight))
    classify = Classify.query.filter(Classify.classify_name == classify_name).first()
    if not classify:
        classify = Classify(classify_name=classify_name,weight=weight)
        db.session.add(classify)
        db.session.commit()

    return redirect(url_for('cms.classify'))


@cms_bluePrint.route('/changeClassify/', methods=['POST'])
@login_required
def changeClassify():
    c_id = request.form.get('c_id')
    weight = request.form.get('weight')
    classify_name = request.form.get('classify_name')
    classify = Classify.query.filter(Classify.id == c_id).first()
    if classify:
        classify.classify_name = classify_name
        classify.weight = weight
        db.session.commit()
        logging.info('修改了网站分类《%s》的信息，新权重为%s'%(classify_name, weight))

    return redirect(url_for('cms.classify'))


@cms_bluePrint.route('/check/', methods=['GET','POST'])
@login_required
def check():
    logging.info('进入了网址检测主页！')
    return render_template('cms/check.html')


@cms_bluePrint.route('/check_url/', methods=['POST'])
@login_required
def check_url():
    webs = WebSite.query.filter(WebSite.website_status == 1).all()
    count = len(webs)
    m = 0
    for web in webs:
        if not Check_Url(web.website_url):
            m+=1
            web.website_status = 0
            db.session.commit()
    logging.info('网址检测结果：共检测%s个正在上线的网址,无效的有%s个,已进行下线处理！'%(count,m))
    return jsonify({'code':200,'msg':'共%s个上线网址,无效的有%s个,已进行下线处理！'%(count,m)})


@cms_bluePrint.route('/account/')
@login_required
def account():
    logging.info('进入CMS用户管理主页')
    users = User.query.all()
    content = {
        'users': users
    }
    return render_template('cms/account.html', **content)


@cms_bluePrint.route('/account/do_act/<int:id>')
@login_required
def do_act(id):
    """
    激活用户账号的视图函数
    :return:
    """
    user = User.query.filter_by(id=id).first()
    user.enabled = 1
    logging.info('CMS用户《{}》进行了激活'.format(user.realname))
    db.session.commit()
    return redirect(url_for('cms.account'))


@cms_bluePrint.route('/account/undo_act/<int:id>')
def undo_act(id):
    """
    封停用户账号的视图函数
    :return:
    """
    user = User.query.filter_by(id=id).first()
    user.enabled = 0
    db.session.commit()
    logging.info('CMS用户《{}》进行了封禁'.format(user.realname))
    return redirect(url_for('cms.account'))

