from flask_cli import FlaskCLI
from flask_migrate import Migrate
from Guide import create_app
from exts import db
from cms.models import User,Role,roles
from common.models import Classify, WebSite

app = create_app()
# 数据库迁移
FlaskCLI(app)
Migrate(app, db)

if __name__ == '__main__':
    app.run()