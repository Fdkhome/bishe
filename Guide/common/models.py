import datetime

from exts import db


class Classify(db.Model):
    '''
    分类：网址分类的类别表
    '''
    __tablename__ = 'classify'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    weight = db.Column(db.Integer, default=0) # 权重
    classify_name = db.Column(db.String(50),nullable=False, unique=True) # 分类名称
    create_time = db.Column(db.DateTime, default=datetime.datetime.now)

    webs = db.relationship('WebSite', backref='classify')


class WebSite(db.Model):
    '''
    网址表
    '''
    __tablename__ = 'website'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    website_name = db.Column(db.String(50),nullable=False) # 网站名称
    website_url = db.Column(db.String(255),nullable=False) # 网站地址
    website_intro = db.Column(db.String(100),nullable=False) # 网站介绍
    website_status = db.Column(db.Boolean, default=0) # 网站状态，默认未上线，0未上线，1上线
    create_time = db.Column(db.DateTime, default=datetime.datetime.now)

    website_classify_id = db.Column(db.Integer, db.ForeignKey('classify.id'))

