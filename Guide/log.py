# coding:utf-8
"""
    日志配置， 在create_app中调用
"""
import logging
import logging.handlers

import os


def init_log(log_path,
             logger=None,
             level=logging.DEBUG,
             when="D",
             backup=7,
             format="%(levelname)s: %(asctime)s: %(filename)s:%(lineno)d * %(thread)d %(message)s",
             datefmt="%m-%d %H:%M:%S"):
    """
    init_log - initialize applog module

    Args:
      log_path      - Log file path prefix.
                      Log data will go to two files: log_path.applog and log_path.applog.wf
                      Any non-exist parent directories will be created automatically
      logger        - default using logging.getLogger()
      level         - msg above the level will be displayed
                      DEBUG < INFO < WARNING < ERROR < CRITICAL
                      the default value is logging.INFO
      when          - how to split the applog file by time interval
                      'S' : Seconds
                      'M' : Minutes
                      'H' : Hours
                      'D' : Days
                      'W' : Week day
                      default value: 'D'
      format        - format of the applog
                      default format:
                      %(levelname)s: %(asctime)s: %(filename)s:%(lineno)d * %(thread)d %(message)s
                      INFO: 12-09 18:02:42: applog.py:40 * 139814749787872 HELLO WORLD
      backup        - how many backup file to keep
                      default value: 7

    Raises:
        OSError: fail to create applog directories
        IOError: fail to open applog file
    """
    # 取得日志对象，设置日志级别
    formatter = logging.Formatter(format, datefmt)
    if not logger:
        logger = logging.getLogger()
    logger.setLevel(level)
    # 日志保存目录
    print("Guide_log dir:" + log_path)
    # log_dir = pathlib.Path(log_path)
    log_dir = os.path.join(os.path.dirname(__file__),log_path)
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir,mode=0o777)
    # set applog handler
    handler = logging.handlers.TimedRotatingFileHandler(log_dir + "/guide.log",
                                                        when=when,
                                                        backupCount=backup,
                                                        encoding='utf-8')
    handler.setLevel(level)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    # set applog handler 单独保存 warning 日志
    handler = logging.handlers.TimedRotatingFileHandler(log_dir + "/guide.log.wf",
                                                        when=when,
                                                        backupCount=backup,
                                                        encoding='utf-8')
    handler.setLevel(logging.WARNING)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
