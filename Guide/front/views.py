from flask import Blueprint, render_template, redirect, url_for, abort
from sqlalchemy import and_
from common.models import WebSite, Classify
from exts import db

front_bluePrint = Blueprint('front',__name__,static_folder='templates/front')


@front_bluePrint.route('/')
def index():

    classifys = Classify.query.order_by(Classify.weight.desc()).order_by(Classify.create_time.desc()).all()
    datas = {}
    for classify in classifys:
        webs = WebSite.query.filter(and_(WebSite.website_classify_id == classify.id, WebSite.website_status == 1)).limit(20).all()
        datas[classify] = webs
    content = {
        'datas' : datas
    }
    return render_template('front/index.html', **content)


@front_bluePrint.route('/detail/<int:c_id>')
def detail(c_id):
    classifys = Classify.query.filter(Classify.id == c_id).all()
    if not classifys:
        abort(404)
    datas = {}
    for classify in classifys:
        webs = WebSite.query.filter(
            and_(WebSite.website_classify_id == classify.id, WebSite.website_status == 1)).limit(20).all()
        datas[classify] = webs
    content = {
        'datas' : datas,
        'title' : classifys[0].classify_name
    }
    return render_template('front/index.html', **content)