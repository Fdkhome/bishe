from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask_login import LoginManager
from flask_principal import Principal, Permission, RoleNeed

bcrypt = Bcrypt()
# cli = FlaskCLI()
db = SQLAlchemy()
mail = Mail()

# 登录检查
login_manager = LoginManager()
login_manager.login_view = "cms.login"  # 指定 登录页的视图
login_manager.login_message = "请登录后访问此页面"  # 用户登录提示
login_manager.session_protection = "strong"  # 防止用户恶意篡改 cookie


# 权限角色，两个角色，default和admin
principals = Principal()
admin_permission = Permission(RoleNeed('admin'))
default_permission = Permission(RoleNeed('default'))


@login_manager.user_loader
def load_user(userid):
    """
    Flask_Login使用该函数检查给定id是否对应正确的用户对象
    :param userid: 用户id
    :return: User对象
    """
    from cms.models import User
    return User.query.get(userid)

