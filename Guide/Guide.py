from flask import Flask, render_template
from flask_login import current_user
from flask_principal import identity_loaded, UserNeed, RoleNeed

from cms import cms_bluePrint
from front import front_bluePrint
from flask_wtf import CSRFProtect
from exts import db, mail, bcrypt, principals, login_manager
import config
import log


def create_app():
    app = Flask(__name__)
    app.config.from_object(config)
    app.register_blueprint(cms_bluePrint)
    app.register_blueprint(front_bluePrint)

    db.init_app(app)
    bcrypt.init_app(app)
    principals.init_app(app)
    login_manager.init_app(app)
    # 初始化log日志
    log.init_log('guide_log')
    CSRFProtect(app)

    @identity_loaded.connect_via(app)
    def on_identity_loaded(sender, identity):
        # 设置 identity user 对象
        identity.user = current_user
        # 添加 UserNeed 给 identity
        if hasattr(current_user, 'id'):
            identity.provides.add(UserNeed(current_user.id))
        # 用户的role 给 identity
        if hasattr(current_user, 'roles'):
            for role in current_user.roles:
                identity.provides.add(RoleNeed(role.name))


    return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)

